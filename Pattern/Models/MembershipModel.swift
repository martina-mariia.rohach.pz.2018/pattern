//
//  MembershipModel.swift
//  Pattern
//
//  Created by Martina on 24.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class MembershipModel: Object {
    dynamic var title: String = ""
    dynamic var numberOfTimes: Int = 0
    dynamic var useTimes: Int = 0
}
