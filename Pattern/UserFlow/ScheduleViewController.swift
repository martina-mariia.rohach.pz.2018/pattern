//
//  ScheduleViewController.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit
import RealmSwift

class ScheduleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SendComplete, BookSubscriber {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var schedule: Schedule?
    var lastSelection: IndexPath!
    var strategy: Strategy?
    let bookSubscriberManager = BookSubscriberManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookSubscriberManager.add(subscriber: self)
        bookSubscriberManager.add(subscriber: self.navigationController as! BookSubscriber)
        strategy = GetPPStrategy(vc: self)
        UserDefaults.standard.set(true, forKey: "trainee")
        let factory = ScheduleFactory()
        schedule = factory.createSchedule()
        
        tableView.register(UINib(nibName: "\(ScheduleItemTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(ScheduleItemTableViewCell.self)")
        tableView.reloadData()
        let facade = ImageFilterFacade()
        facade.applyTo(image: UIImage(named: "stretching")!)
        apply(theme: NightButtonTheme(), for: button)
        apply(theme: NightButtonTheme(), for: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        strategy?.presentAlert()
    }
    
    func apply<T: Theme>(theme: T, for component: Component) {
        component.accept(theme: theme)
    }
    
    @IBAction func bookWorkout(_ sender: Any) {
        for i in 0...schedule!.items.count - 1 {
            if tableView.cellForRow(at: IndexPath(row: i, section: 0))?.accessoryType == .checkmark {
                let alert = UIAlertController(title: "Are you sure?", message: "Do you want to book \(schedule!.items[i].title) on \(schedule!.items[i].day)?", preferredStyle: .actionSheet)
                let action = UIAlertAction(title: "Yes", style: .default) { (_) in
                    let command = BookInOneClickCommand()
                    command.execute()
                    self.bookSubscriberManager.remove()
                    self.bookSubscriberManager.add(workout: (self.schedule?.items[i])!)
                }
                let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
                alert.addAction(action)
                alert.addAction(cancel)
                present(alert, animated: true, completion: nil)
                tableView.cellForRow(at: IndexPath(row: i, section: 0))?.accessoryType = .none
            }
        }
    }
    
    func update() {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedule?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ScheduleItemTableViewCell.self)", for: indexPath) as? ScheduleItemTableViewCell else { return UITableViewCell() }
        cell.configure(with: schedule!.items[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.lastSelection != nil {
            tableView.cellForRow(at: self.lastSelection)?.accessoryType = .none
        }

        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark

        self.lastSelection = indexPath

        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func accept(changed workout: ScheduleItem?) {
        if let work = workout {
            guard let index = schedule?.items.firstIndex(where: { (item) -> Bool in
                return item.title == work.title && item.time == work.time && item.day == work.day
            }) else { return }
            guard let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? ScheduleItemTableViewCell else { return }
            cell.backgroundColor = UIColor(red: 204/255.0, green: 255/255.0, blue: 204/255.0, alpha: 1.0)
        } else {
            for i in 0..<schedule!.items.count {
                guard let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as? ScheduleItemTableViewCell else { return }
                cell.backgroundColor = UIColor.white
            }
        }
    }
}
