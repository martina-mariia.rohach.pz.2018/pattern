//
//  ScheduleFactory.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

//ABSTRACT FACTORY
class ScheduleFactory {
    private var scheduleFactory: AbstractFactory?
    
    func initScheduleFactory() {
        if scheduleFactory != nil { return }
        if UserDefaults.standard.bool(forKey: "coach") {
            scheduleFactory = CoachScheduleFactory()
        } else {
            scheduleFactory = TraineeScheduleFactory()
        }
    }
    
    func createSchedule() -> Schedule {
        initScheduleFactory()
        return scheduleFactory!.createSchedule()
    }
}
