//
//  JsonDecoder.swift
//  Pattern
//
//  Created by Martina on 13.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

//
final class Server {
    private let session = URLSession.shared
    
    static let shared = Server()              //SINGLETON
    private init() {}

    func doRequest<T: Decodable>(url: String?, result: @escaping (Result<T, Error>) -> Void) {
        if let path = Bundle.main.path(forResource: url, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let string = String(data: data, encoding: .isoLatin1) ?? ""
                let utf8Data = Data(string.utf8)
                //SERIALIZATION
                let results = try JSONDecoder().decode(T.self, from: utf8Data)
                result(.success(results))
            } catch let error {
                result(.failure(error))
            }
        }
    }
}
