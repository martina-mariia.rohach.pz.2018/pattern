//
//  PPOriginator.swift
//  Pattern
//
//  Created by Martina on 24.05.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

//MEMENTO
protocol Memento {
    var activities: [Activity] { get }
    var date: Date { get }
    var description: String { get }
}


extension PersonalProgramModel {
    
    var memento: Memento {
        return PPMemento(activities: activities)
    }

    func restore(with memento: Memento) {
        guard let ppMemento = memento as? PPMemento else { return }

        activities = ppMemento.activities
    }

    struct PPMemento: Memento {
        
        var activities: [Activity]
        let date = Date()
        
        var description: String {
            let time = Calendar.current.dateComponents([.hour, .minute, .second, .nanosecond],
                                                       from: date)
            var descr = "\nActivities: \n"
            activities.forEach { (activity) in
                descr.append("\(activity.title)\n")
            }
            return "\(descr) Date: \(time.description)\n"
        }
    }
}

class UndoStack: CustomStringConvertible {

    private lazy var mementos = [Memento]()
    private let personalProgram: PersonalProgramModel

    init(_ personalProgram: PersonalProgramModel) {
        self.personalProgram = personalProgram
    }

    func save() {
        mementos.append(personalProgram.memento)
    }

    func undo() {
        guard !mementos.isEmpty else { return }
        personalProgram.restore(with: mementos.removeLast())
    }
    
    func isEmpty() -> Bool {
        return mementos.isEmpty
    }
    
    func printAll() {
        mementos.forEach { (memento) in
            print(memento.description)
        }
    }

    var description: String {
        return mementos.reduce("", { $0 + $1.description })
    }
}
